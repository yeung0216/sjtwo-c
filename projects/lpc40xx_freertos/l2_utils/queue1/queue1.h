#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

/* In this part, the queue memory is statically defined
 * and fixed at compile time for 100 uint8s
 */

#define MAX_SIZE 100

typedef struct {
  uint8_t queue_memory[MAX_SIZE];

  int count;
  int push_index;
  int pull_index;
  // TODO: Add more members as needed
} queue_s;

// This should initialize all members of queue_s
void queue1__init(queue_s *queue);

/// @returns false if the queue is full
bool queue1__push(queue_s *queue, uint8_t push_value);

/// @returns false if the queue was empty
bool queue1__pop(queue_s *queue, uint8_t *pop_value);

size_t queue1__get_item_count(const queue_s *queue);

size_t queue1__print(const queue_s *queue);