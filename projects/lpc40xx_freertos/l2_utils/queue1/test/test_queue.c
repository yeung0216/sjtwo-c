#include "queue1.h"
#include "unity.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

static queue_s queue;

void setUp(void) { queue1__init(&queue); }

void test_comprehensive(void) {
  const size_t max_queue_size = MAX_SIZE; // Change if needed

  for (size_t item = 0; item < max_queue_size; item++) {
    const uint8_t item_pushed = (uint8_t)item;
    TEST_ASSERT_TRUE(queue1__push(&queue, item_pushed));
    TEST_ASSERT_EQUAL(item + 1, queue1__get_item_count(&queue));
  }

  // Should not be able to push anymore
  TEST_ASSERT_FALSE(queue1__push(&queue, 123));
  TEST_ASSERT_EQUAL(max_queue_size, queue1__get_item_count(&queue));

  // Pull and verify the FIFO order
  for (size_t item = 0; item < max_queue_size; item++) {
    uint8_t popped_value = 0;
    TEST_ASSERT_TRUE(queue1__pop(&queue, &popped_value));
    TEST_ASSERT_EQUAL((uint8_t)item, popped_value);
  }

  // Test wrap-around case
  const uint8_t pushed_value = 123;
  TEST_ASSERT_TRUE(queue1__push(&queue, pushed_value));
  uint8_t popped_value = 0;
  TEST_ASSERT_TRUE(queue1__pop(&queue, &popped_value));
  TEST_ASSERT_EQUAL(pushed_value, popped_value);

  TEST_ASSERT_EQUAL(0, queue1__get_item_count(&queue));
  TEST_ASSERT_FALSE(queue1__pop(&queue, &popped_value));
}

void test_push_pop_over(void) {
  const size_t max_queue_size = MAX_SIZE; // Change if needed

  for (size_t item = 0; item < max_queue_size; item++) {
    const uint8_t item_pushed = (uint8_t)item;
    TEST_ASSERT_TRUE(queue1__push(&queue, item_pushed));
    TEST_ASSERT_EQUAL(item + 1, queue1__get_item_count(&queue));
  }

  // Should not be able to push anymore
  TEST_ASSERT_FALSE(queue1__push(&queue, 123));
  TEST_ASSERT_EQUAL(max_queue_size, queue1__get_item_count(&queue));

  // Pull Half and verify the FIFO order
  for (size_t item = 0; item < max_queue_size - 50; item++) {
    uint8_t popped_value = 0;
    TEST_ASSERT_TRUE(queue1__pop(&queue, &popped_value));
    TEST_ASSERT_EQUAL((uint8_t)item, popped_value);
  }

  TEST_ASSERT_EQUAL(max_queue_size - 50, queue1__get_item_count(&queue));

  // Push another Half
  for (size_t item = 100; item < max_queue_size + 50; item++) {
    const uint8_t item_pushed = (uint8_t)item;
    // printf('item_pushed: %d\n', item_pushed);
    TEST_ASSERT_TRUE(queue1__push(&queue, item_pushed));
    TEST_ASSERT_EQUAL(item - 50 + 1, queue1__get_item_count(&queue));
  }

  // Should not be able to push anymore
  TEST_ASSERT_FALSE(queue1__push(&queue, 123));
  TEST_ASSERT_EQUAL(max_queue_size, queue1__get_item_count(&queue));

  // Pull all and verify the FIFO order
  for (size_t item = 50; item < max_queue_size + 50; item++) {
    uint8_t popped_value = 0;
    TEST_ASSERT_TRUE(queue1__pop(&queue, &popped_value));
    TEST_ASSERT_EQUAL((uint8_t)item, popped_value);
  }

  uint8_t popped_value;
  // pull empty
  TEST_ASSERT_EQUAL(0, queue1__get_item_count(&queue));
  TEST_ASSERT_FALSE(queue1__pop(&queue, &popped_value));
}

void simple_push_pop() {
  uint8_t pushed_value;
  uint8_t popped_value;
  uint8_t expect;
  pushed_value = 123;
  TEST_ASSERT_TRUE(queue1__push(&queue, pushed_value));
  pushed_value = 124;
  TEST_ASSERT_TRUE(queue1__push(&queue, pushed_value));
  pushed_value = 125;
  TEST_ASSERT_TRUE(queue1__push(&queue, pushed_value));

  TEST_ASSERT_EQUAL(3, queue1__get_item_count(&queue));

  expect = 123;
  TEST_ASSERT_TRUE(queue1__pop(&queue, &popped_value));
  TEST_ASSERT_EQUAL(expect, popped_value);
  expect = 124;
  TEST_ASSERT_TRUE(queue1__pop(&queue, &popped_value));
  TEST_ASSERT_EQUAL(expect, popped_value);
  expect = 125;
  TEST_ASSERT_TRUE(queue1__pop(&queue, &popped_value));
  TEST_ASSERT_EQUAL(expect, popped_value);

  TEST_ASSERT_EQUAL(0, queue1__get_item_count(&queue));
}