
#include "queue1.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

// This should initialize all members of queue_s
void queue1__init(queue_s *queue) {
  queue->count = 0;
  queue->pull_index = MAX_SIZE - 1;
  queue->push_index = 0;
}
/// @returns false if the queue is full
bool queue1__push(queue_s *queue, uint8_t push_value) {
  //  printf('checl: %d\n', push_value);
  if (queue->count == MAX_SIZE) {
    printf("Queue is full. Cannot enqueue %d.\n", push_value);
    return false;
  }

  queue->pull_index = (queue->pull_index + 1) % MAX_SIZE;
  queue->queue_memory[queue->pull_index] = push_value;
  queue->count++;

  return true;
}

/// @returns false if the queue was empty
bool queue1__pop(queue_s *queue, uint8_t *pop_value) {

  if (queue->count == 0) {
    printf("Queue is empty. Cannot dequeue.\n");
    return false; // Return some default value indicating failure
  }

  int item = queue->queue_memory[queue->push_index];
  queue->push_index = (queue->push_index + 1) % MAX_SIZE;
  queue->count--;

  *pop_value = item;
  return true;
};

size_t queue1__get_item_count(const queue_s *queue) { return queue->count; }

size_t queue1__print(const queue_s *queue) {
  for (int i = 0; i < MAX_SIZE; i++) {
    printf("i: %d\n", queue->queue_memory[i]);
  }
}
