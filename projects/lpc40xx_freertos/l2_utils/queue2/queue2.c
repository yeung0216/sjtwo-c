
#include "queue2.h"
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

// This should initialize all members of queue_s
void queue2__init(queue_s *queue, void *static_memory_for_queue, size_t static_memory_size_in_bytes) {
  queue->static_memory_for_queue = static_memory_for_queue;
  queue->static_memory_size_in_bytes = static_memory_size_in_bytes;

  queue->count = 0;
  int size = (int)static_memory_size_in_bytes;
  queue->pull_index = size - 1;

  queue->push_index = 0;
}

/// @returns false if the queue is full
bool queue2__push(queue_s *queue, uint8_t push_value) {

  int size = (int)queue->static_memory_size_in_bytes;
  if (queue->count == size) {
    printf("Queue is full. Cannot enqueue %d.\n", push_value);
    return false;
  }
  queue->pull_index = (queue->pull_index + 1) % size;
  queue->static_memory_for_queue[queue->pull_index] = push_value;
  queue->count++;

  return true;
}

/// @returns false if the queue was empty
bool queue2__pop(queue_s *queue, uint8_t *pop_value) {
  int size = (int)queue->static_memory_size_in_bytes;
  if (queue->count == 0) {
    printf("Queue is empty. Cannot dequeue.\n");
    return false; // Return some default value indicating failure
  }

  int item = queue->static_memory_for_queue[queue->push_index];
  queue->push_index = (queue->push_index + 1) % size;
  queue->count--;

  *pop_value = item;
  return true;
};

size_t queue2__get_item_count(const queue_s *queue) { return queue->count; }
