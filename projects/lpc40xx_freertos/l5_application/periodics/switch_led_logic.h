#pragma once

#include "gpio.h"
#include <stdbool.h>
#include <stdint.h>

void switch_led_logic__initialize(void);

void switch_led_logic__run_once(void);

void switch_led_logic__run_hundred(void);

void switch_led_logic__run_thousand(void);